FROM ubuntu:focal-20231003
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt update -y && apt install -y nodejs npm iputils-ping net-tools postgresql-client
RUN su - && addgroup app && useradd app -g app --create-home
COPY . /home/app
RUN cd /home/app && npm install
RUN chown -R app:app /home/app
USER app
WORKDIR /home/app
EXPOSE 8080
CMD [ "node", "/home/app/server.js" ]
